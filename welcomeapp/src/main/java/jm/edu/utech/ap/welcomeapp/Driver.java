package jm.edu.utech.ap.welcomeapp;
import java.util.Scanner;

import jm.edu.utech.ap.welcomelib.IWelcomeService;

public class Driver {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		IWelcomeService service = new WelcomeService();
		System.out.println("Please enter your name");
		String name = input.nextLine();
		System.out.println(service.getWelcomeMessage(name));
		input.close();

	}

}
